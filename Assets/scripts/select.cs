﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class select : MonoBehaviour {

    //detectar si se ha seleccionado algo ya
    public bool pj1selected, pj2selected = false;

    public bool[] pj1_sel = new bool[2];
    public bool[] pj2_sel = new bool[2];

    //imagenes que se muetra en pj1 y 2 al ya haber seleccionado
    public GameObject[] pj1_imgs = new GameObject[2];
    public GameObject[] pj2_imgs = new GameObject[2];

    //botones para seleccionar pj
    public Button[] Char_Buttons = new Button[2];

    public GameObject play_button;

    void Start () {

        Char_Buttons[0].GetComponent<Button>();
        Char_Buttons[1].GetComponent<Button>();

    }

    void Update () {
        //asignar img segun el bool activo
        if (pj1_sel[0]) {
            pj1_imgs[0].SetActive(true);
            pj1_imgs[1].SetActive(false);
        }
        if (pj1_sel[1]) {
            pj1_imgs[1].SetActive(true);
            pj1_imgs[0].SetActive(false);
        }
        if (pj2_sel[0]) {
            pj2_imgs[0].SetActive(true);
            pj2_imgs[1].SetActive(false);
        }
        if (pj2_sel[1]) {
            pj2_imgs[1].SetActive(true);
            pj2_imgs[0].SetActive(false);
        }

        if ((pj1selected) && (pj2selected)) {
            play_button.SetActive(true);
        } else {
            play_button.SetActive(false);
        }

    }
    public void PJ1_lock() {
        //como pj2 de inicio está true para que no se elijan characters, cambio tanto el 1 como el 2
        if ((pj1_sel[0]) || (pj1_sel[1])) {
            pj1selected = true;
            pj2selected = false;
        }
    }
    public void PJ2_lock() {
        if ((pj2_sel[0]) || (pj2_sel[1])) {
            pj2selected = true;
        }
    }


    public void Pj1_button0() {

        /*si pj1 no tiene nada seleccionado y pulsa el btn 0, se activa alpha1 y a la vez si pj1 si tiene algo
        seleccionado pero pj2 no, alpha1 se activa a pj2.
         */

        if (!pj1selected) {
            pj1_sel[0] = true;
            pj1_sel[1] = false;
        }
        if ((pj1selected) && (!pj2selected)) {
            pj2_sel[0] = true;
            pj2_sel[1] = false;
        }
    }

    public void Pj1_button1() {
        if (!pj1selected) {

            pj1_sel[1] = true;
            pj1_sel[0] = false;
            
        }
        if ((pj1selected) && (!pj2selected)) {

            pj2_sel[1] = true;
            pj2_sel[0] = false;

        }
    }
}
