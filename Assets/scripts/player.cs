﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : MonoBehaviour {

    public float hp_actual;
    public float hp_default;
    public GameObject barra_hp;

    //daño de otro pj
    public float golpe;
    public int golpes = 1;

    public float xSpeed;

    public float jumpValue;
    public float maxJumpValue;
    public float gravValue;

    public bool jumping;
    public int saltos;
    public int saltosDefault;
    public bool isGrounded;

    public KeyCode up, left, down, right, ataque, especial, proteger;

    //script de 00script
    public GameObject scripts;

    public Rigidbody2D rb;
    public SpriteRenderer sr;
    public Animator anim;

    public bool atacando;
    public BoxCollider2D atacando_coll;
    public float timer_ataque;
    public float cd_ataque = 1.0f;

    public bool muerto = false;

    void Start() {

        //valores default y GetComponents iniciales
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        scripts = GameObject.Find("00scripts_Juego");
        saltos = saltosDefault;
        atacando_coll = transform.GetChild(0).GetComponent<BoxCollider2D>();
        timer_ataque = cd_ataque;
        hp_actual = hp_default;

        //Obtener las barras de vida
        if (this.gameObject.name == "Player1") {
            barra_hp = GameObject.Find("PJ1_vida");
        }

        if (this.gameObject.name == "Player2") {
            barra_hp = GameObject.Find("PJ2_vida");

        }

    }

    void Update() {
        
        //Mostrar vida a partir de las barras
        barra_hp.GetComponent<Image>().fillAmount = hp_actual / hp_default;

        //actualizar controles segun la pantalla controles
        if (this.gameObject.name == "Player1") {
            up = scripts.GetComponent<cosas>().keys["P1_Up"];
            down = scripts.GetComponent<cosas>().keys["P1_Down"];
            left = scripts.GetComponent<cosas>().keys["P1_Left"];
            right = scripts.GetComponent<cosas>().keys["P1_Right"];
            ataque = scripts.GetComponent<cosas>().keys["P1_Ataque"];
            especial = scripts.GetComponent<cosas>().keys["P1_Especial"];
            proteger = scripts.GetComponent<cosas>().keys["P1_Proteger"];

            if (hp_actual <= 0) {
                muerto = true;
                scripts.GetComponent<cosas>().player1_muerto=true;
                Destroy(this.gameObject);
            }
        }
        if (this.gameObject.name == "Player2") {
            up = scripts.GetComponent<cosas>().keys["P2_Up"];
            down = scripts.GetComponent<cosas>().keys["P2_Down"];
            left = scripts.GetComponent<cosas>().keys["P2_Left"];
            right = scripts.GetComponent<cosas>().keys["P2_Right"];
            ataque = scripts.GetComponent<cosas>().keys["P2_Ataque"];
            especial = scripts.GetComponent<cosas>().keys["P2_Especial"];
            proteger = scripts.GetComponent<cosas>().keys["P2_Proteger"];

            if (hp_actual <= 0) {
                muerto = true;
                scripts.GetComponent<cosas>().player2_muerto = true;
                Destroy(this.gameObject);
            }
        }

        //movimiento tanto aire como suelo
        if (!jumping) {
            if (Input.GetKey(right)) {
                //movimiento
                transform.Translate(1 * xSpeed * Time.deltaTime, 0, 0);

                //giro de cuerpo
                this.gameObject.transform.localScale = new Vector3(-1.5f, 1.5f, 1);

            }
            if (Input.GetKey(left)) {
                //movimiento
                transform.Translate(-1 * xSpeed * Time.deltaTime, 0, 0);

                //giro de cuerpo
                this.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
            }
        } else {
            if (Input.GetKey(right)) {
                //movimiento
                transform.Translate((1 * xSpeed * Time.deltaTime) / 1.5f, 0, 0);
                //giro
                this.gameObject.transform.localScale = new Vector3(-1.5f, 1.5f, 1);
            }
            if (Input.GetKey(left)) {
                //movimiento
                transform.Translate((-1 * xSpeed * Time.deltaTime) / 1.5f, 0, 0);
                //giro
                this.gameObject.transform.localScale = new Vector3(1.5f, 1.5f, 1);
            }
        }

        //Movimiento Vertical
        if (isGrounded) {
            saltos = saltosDefault;
        }

        if ((Input.GetKeyDown(up)) && (saltos > 0)) {

            //mayor (saltando)
            if (rb.velocity.y > 0) {
                rb.AddForce(Vector3.up * jumpValue, ForceMode2D.Impulse);

                //menor (cayendo)
            } else if (rb.velocity.y < 0) {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y + (rb.velocity.y * (-1)));
                rb.AddForce(Vector3.up * jumpValue, ForceMode2D.Impulse);
                //sin salto
            } else {
                rb.AddForce(Vector3.up * jumpValue, ForceMode2D.Impulse);
            }

            jumping = true;
            isGrounded = false;
            saltos--;
        }

        //Salto extendido
        if (Input.GetKey(up)) {

            if (jumpValue < maxJumpValue) {

                rb.AddForce(Vector3.up * maxJumpValue, ForceMode2D.Force);
            }
        }
        //caida más rápida
        if (rb.velocity.y <= 0) {
            rb.AddForce(Vector3.down * gravValue, ForceMode2D.Force);
        }

        if ((Input.GetKey(down)) && (jumping)) {
            rb.AddForce(Vector3.down * (jumpValue / 3f), ForceMode2D.Impulse);
        }

        //detecta que está en la misma altura, significa que está quieto en algún sitio
        if (jumping) {
            if (rb.velocity.y == 0) {
                jumping = false;
                isGrounded = true;
            }
        }

        //Si ataca que ejecute animacion y haga cooldown
        if ((Input.GetKeyDown(ataque)) && (timer_ataque == cd_ataque)) {
            atacando = true;
            anim.SetBool("Ataque", atacando);
        }
        if (atacando) {
            atacando_coll.enabled = true;
            timer_ataque -= Time.deltaTime;
            if (timer_ataque < 0) {
                timer_ataque = cd_ataque;
                atacando = false;
                anim.SetBool("Ataque", atacando);
            }
        } else if (!atacando) {
            atacando_coll.enabled = false;
        }

    }

    public void OnTriggerEnter2D(Collider2D other) {

        //detecte colision con puño y reste su daño a mi vida
        if ((other.gameObject.name == "Puño") && (golpes == 1)) {
            golpes--;
            golpe = other.gameObject.GetComponent<player_puño>().ataque_actual;
            hp_actual -= golpe;
            Debug.Log(this.gameObject.name + " tiene " + hp_actual + " por golpe de " + other.transform.parent.gameObject.name);
        }
    }

    public void OnTriggerExit2D(Collider2D other) {
        //si chocas con el puño de alguien te hase pupah.
        if (other.gameObject.name == "Puño") {
            golpes=1;
            Debug.Log("Salió de colisión");
        }
    }


    
}
