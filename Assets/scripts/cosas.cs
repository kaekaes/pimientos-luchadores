﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class cosas : MonoBehaviour {

    public GameObject Player1;
    public GameObject Player2;
    public GameObject Canvas;
    public GameObject[] Canvas_ventanas = new GameObject[4];
    public bool si_o_no;

    //hacer que keys contenga una letra y un keycode
    public Dictionary<string, KeyCode> keys = new Dictionary<string, KeyCode>();
    public Text Controles_PJ1Texto_Up, Controles_PJ1Texto_Down, Controles_PJ1Texto_Left, Controles_PJ1Texto_Right, Controles_PJ1Texto_Ataque, Controles_PJ1Texto_Especial, Controles_PJ1Texto_Proteger;
    public KeyCode Controles_PJ1KC_Up, Controles_PJ1KC_Down, Controles_PJ1KC_Left, Controles_PJ1KC_Right, Controles_PJ1KC_Ataque, Controles_PJ1KC_Especial, Controles_PJ1KC_Proteger;
    public Text Controles_PJ2Texto_Up, Controles_PJ2Texto_Down, Controles_PJ2Texto_Left, Controles_PJ2Texto_Right, Controles_PJ2Texto_Ataque, Controles_PJ2Texto_Especial, Controles_PJ2Texto_Proteger;
    public KeyCode Controles_PJ2KC_Up, Controles_PJ2KC_Down, Controles_PJ2KC_Left, Controles_PJ2KC_Right, Controles_PJ2KC_Ataque, Controles_PJ2KC_Especial, Controles_PJ2KC_Proteger;
    public GameObject currentKey;
    private Color32 selected = new Color32(255, 150, 150, 255);
    private Color32 normal = new Color32(255, 255, 255, 255);

    public bool enpartida;
    public bool player1_muerto, player2_muerto;
    public Text Ganador_Texto;
    public GameObject Canvas_Fin;

    void Start () {
        Player1 = GameObject.Find("Player1");
        Player2 = GameObject.Find("Player2");

        //poner que controles_PJ... sean los que tiene el player 1
        Controles_PJ1KC_Down = KeyCode.S;
        Controles_PJ1KC_Up = KeyCode.W;
        Controles_PJ1KC_Left = KeyCode.A;
        Controles_PJ1KC_Right = KeyCode.D;
        Controles_PJ1KC_Ataque = KeyCode.G;
        Controles_PJ1KC_Especial = KeyCode.H;
        Controles_PJ1KC_Proteger = KeyCode.J;

        //player 2
        Controles_PJ2KC_Down = KeyCode.DownArrow;
        Controles_PJ2KC_Up = KeyCode.UpArrow;
        Controles_PJ2KC_Left = KeyCode.LeftArrow;
        Controles_PJ2KC_Right = KeyCode.RightArrow;
        Controles_PJ2KC_Ataque = KeyCode.Delete;
        Controles_PJ2KC_Especial = KeyCode.PageDown;
        Controles_PJ2KC_Proteger = KeyCode.PageUp;

        //Que los keycodes sean strings
        keys.Add("P1_Up", Controles_PJ1KC_Up);
        keys.Add("P1_Down", Controles_PJ1KC_Down);
        keys.Add("P1_Left", Controles_PJ1KC_Left);
        keys.Add("P1_Right", Controles_PJ1KC_Right);
        keys.Add("P1_Ataque", Controles_PJ1KC_Ataque);
        keys.Add("P1_Especial", Controles_PJ1KC_Especial);
        keys.Add("P1_Proteger", Controles_PJ1KC_Proteger);

        Controles_PJ1Texto_Up.text = keys["P1_Up"].ToString();
        Controles_PJ1Texto_Down.text = keys["P1_Down"].ToString();
        Controles_PJ1Texto_Left.text = keys["P1_Left"].ToString();
        Controles_PJ1Texto_Right.text = keys["P1_Right"].ToString();
        Controles_PJ1Texto_Ataque.text = keys["P1_Ataque"].ToString();
        Controles_PJ1Texto_Especial.text = keys["P1_Especial"].ToString();
        Controles_PJ1Texto_Proteger.text = keys["P1_Proteger"].ToString();

        //player 2
        keys.Add("P2_Up", Controles_PJ2KC_Up);
        keys.Add("P2_Down", Controles_PJ2KC_Down);
        keys.Add("P2_Left", Controles_PJ2KC_Left);
        keys.Add("P2_Right", Controles_PJ2KC_Right);
        keys.Add("P2_Ataque", Controles_PJ2KC_Ataque);
        keys.Add("P2_Especial", Controles_PJ2KC_Especial);
        keys.Add("P2_Proteger", Controles_PJ2KC_Proteger);

        Controles_PJ2Texto_Up.text = keys["P2_Up"].ToString();
        Controles_PJ2Texto_Down.text = keys["P2_Down"].ToString();
        Controles_PJ2Texto_Left.text = keys["P2_Left"].ToString();
        Controles_PJ2Texto_Right.text = keys["P2_Right"].ToString();
        Controles_PJ2Texto_Ataque.text = keys["P2_Ataque"].ToString();
        Controles_PJ2Texto_Especial.text = keys["P2_Especial"].ToString();
        Controles_PJ2Texto_Proteger.text = keys["P2_Proteger"].ToString();
    }

    void Update() {
        if (player1_muerto == true) {
            enpartida = false;
            Canvas_Fin.SetActive(true);
            Ganador_Texto.text = "Player 2 wins!";
            Player2.GetComponent<player>().enabled = false;
        }
        if (player2_muerto == true) {
            enpartida = false;
            Canvas_Fin.SetActive(true);
            Ganador_Texto.text = "Player 1 wins!";
            Player1.GetComponent<player>().enabled = false;
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            si_o_no = !si_o_no;
            Canvas_ventanas[0].SetActive(false);
            Canvas_ventanas[1].SetActive(true);
            Canvas_ventanas[2].SetActive(false);
            Canvas_ventanas[3].SetActive(true);
            Player1.GetComponent<player>().enabled = !si_o_no;
            Player2.GetComponent<player>().enabled = !si_o_no;
        }
        Canvas.SetActive(si_o_no);
    }
    
    void OnGUI() {
        if (currentKey != null) {
            Event e = Event.current;
            if (e.isKey) {
                keys[currentKey.name] = e.keyCode;
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey.GetComponent<Image>().color = normal;
                currentKey = null;
            }
        }
    }

    public void ChangeKey (GameObject clicked) {
        if (currentKey != null) {
            currentKey.GetComponent<Image>().color = normal;
        }
        currentKey = clicked;
        currentKey.GetComponent<Image>().color = selected;

    }

    public void Restart() {
        SceneManager.LoadScene("Main");
    }
}
